web:
	./venv/bin/fava ./root.bean

balance:
	./venv/bin/bean-report -f text root.bean balances | egrep '^(Assets|Liabilities)'
	./venv/bin/bean-report -f text root.bean equity

