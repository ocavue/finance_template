## 安装方式

```
python3 -v venv venv
. ./venv/bin/active
pip3 install -r requirements.txt
```

## 运行方式

显示当前账户余额

```
make balance
```

打开网页 UI 界面

```
make web
```

## 编写方式

- 修改 meta/opens.bean 注册账户
- 修改 2022/04.bean 记账

## VSCode 插件

推荐使用 VSCode 对文件进行修改，并按照 VSCode 提示安装推荐的插件。
