import sys
import re

RE = r"^([^;]+\s+[\+\-]?)(\d+\.?\d{0,1})(\s+(?:CNY|USD|HKD|EUR).*)$"


def main():
    filename = sys.argv[1]
    print(filename)

    if not filename.endswith(".bean"):
        return
    if filename.endswith('prices.bean'):
        return

    with open(filename, "r") as f:
        lines = f.read().split("\n")

    for i, line in enumerate(lines):
        match = re.match(RE, line)
        if not match:
            continue
        line = match.group(1) + "{:.2f}".format(float(match.group(2))) + match.group(3)
        lines[i] = line
        print(f"fixed line {i+1}")

    with open(filename, "w") as f:
        f.write("\n".join(lines))


if __name__ == "__main__":
    main()
